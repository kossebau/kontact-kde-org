
---
layout: page
title: Get Involved
konqi: /assets/img/konqi-dev.png
---

## Get Involved!

Kontact's source code is spread across many components in many repositories -
it is easy to get lost and overwhelmed if you are new to it. If you want to
get involved in development of Kontact just read the  materials linked from
this page and get in touch with us - we will help you find your way around
our codebase.

**Please start by reading some information on the [KDE PIM Community wiki](https://community.kde.org/KDE_PIM).**

## Junior Jobs

Looking for some simple tasks to get started with improving Kontact? Then check
out our [junior jobs](https://phabricator.kde.org/tag/kde_pim_junior_jobs/).
We have collected a number of tasks that are fairly simple and isolated and don't
require in-depth understanding of the entire codebase and architecture - perfect
entry point into the beautiful world of Kontact.

If you would like to start working on some task, feel free to assign it to yourself
(unless it's already assigned to someone else) and let us know in the comments below
the task.

## Build Kontact from Sources

Follow this [Guide how to get started with Kontact development](https://community.kde.org/KDE_PIM/Development/Start).
It contains links to many other useful wiki pages with lots of information.

We also have a [Docker environment](https://community.kde.org/KDE_PIM/Docker) so that
you can develop and test Kontact in safe isolation from your production setup.

## Get in Touch!

The Kontact developers generally hang around on the [#kontact](irc://freenode.net/#kontact)
and [#akonadi](irc://freenode.net/#akonadi) IRC channels on Freenode. Most
development-related discussions take place on the [kde-pim mailing list](https://mail.kde.org/mailman/listinfo/kde-pim/)
Just join in, say hi and tell us what you would like to help us with!

## Not a Programmer?

Not a problem! There's a plenty of other tasks that you can help us with to
make Kontact better even if you don't know any programming languages!

* [Bug triaging](https://community.kde.org/Guidelines_and_HOWTOs/Bug_triaging) - help us find
  misfiled, duplicated or invalid bug reports in Bugzilla
* [Localization](https://community.kde.org/Get_Involved/translation) - help to translate
  Kontact into your language
* [Documentation](https://community.kde.org/Get_Involved/documentation) - help us improve user
  documentation to make Kontact more friendly for newcomers
* [Promotion](https://community.kde.org/Get_Involved/promotion) - help us promote Kontact
  both online and offline
* Updating wiki - our wiki is a mess full or duplicated and obsolete
  information - help us improve it to make it easier for others to join!
* What ideas might you have? Get in touch!
